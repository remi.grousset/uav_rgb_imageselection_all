"""
List of errors managed by the current module
"""


class DataError(Exception):
    """
    Exception raised whenever data is empty, corrupted or invalid.
    In the case of a parallelized module, this error doesn't kill the overall execution.
    """

    def __init__(self, arg=None):
        self.arg = arg

    def __str__(self):
        if self.arg is None:
            return "Data is empty, invalid or corrupted"
        else:
            return "Data '{0}' is empty, invalid or corrupted".format(self.arg)
