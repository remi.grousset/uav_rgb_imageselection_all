"""
Entry point script for My Module.

TODO: Adds description about My Module
"""
from pathlib import Path
import os
import logging

import cv2
import numpy as np

from module import utils


logger = logging.getLogger('__main__')


def remove_black_stripes(img):
    """
    crop the input image to minimize the black pixels around the microplot. Return the cropped image
    :param img: ndarray containing the image that needed to be cropped
    :return crop: ndarray containing the stitched image
    """
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnt = contours[0]
    x, y, w, h = cv2.boundingRect(cnt)
    crop = img[y:y + h, x:x + w]
    return crop


def calculate_ratio(img):
    # obsolete
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return np.where(gray != 0)[0].shape[0] / (gray.shape[0] * gray.shape[1])


def check_img(img):
    """check if the image is not cropped lengthwise
    :param img: ndarray representing the image of the plot analysed
    return boolean: True if the image is not cropped, False otherwise
    """
    # ratio = calculate_ratio(img)
    img = remove_black_stripes(img)
    top = np.where(img[0, :, 2] != 0)
    bottom = np.where(img[-1, :, 2] != 0)
    result = True if bottom[0].shape[0] + top[0].shape[0] < 250 else False

    if img.shape[0] * img.shape[1] < 100000:  ###### Modif to remove errors from phenoscript
        return False
    return result


def classify_images(images):
    """
    Sort the image according to the "valid_pixel" value in the exported plots data. The valid_pixel informs the percentage of the plot covered by the image
    :param images: list of images contain
    :return dict of ndarrays containing images and info about those
    """
    # Sort with valid_pixel
    sorted_images = []
    for image in images:
        if len(sorted_images) > 0:
            N = 0
            while image["valid_pixel"] < sorted_images[N]["valid_pixel"]:
                N += 1
                if N == len(sorted_images):
                    break

            sorted_images.insert(N, image)

        else:
            sorted_images.append(image)

    # Sort the 100% valid_pixel with camera_zenith
    M = 0
    zenith_sorted_images = []
    while sorted_images[M]["valid_pixel"] > 99.85:
        if len(zenith_sorted_images) > 0:
            N = 0
            while sorted_images[M]["nadir"] > zenith_sorted_images[N]["nadir"]:
                N += 1
                if N == len(zenith_sorted_images):
                    break

            zenith_sorted_images.insert(N, sorted_images[M])

        else:
            zenith_sorted_images.append(sorted_images[M])

        M += 1
        if M == len(sorted_images):
            break
    sorted_images = np.array(sorted_images, dtype=object)
    sorted_images[:M] = np.array(zenith_sorted_images, dtype=object)
    sorted_images = sorted_images.tolist()

    return sorted_images


def detect_top_bottom_black_bars(sorted_images):
    """
    Detect if the plot is not complete on the bottom or on the top of the image. Indeed, those images are discarded
    :param sorted_image: dict with ndarrays containing images and info about those
    return dict of ndarrays containing images and info about those
    """
    for sorted_image in sorted_images:
        if check_img(sorted_image["image"]):
            sorted_image["is_cropped"] = True
    return sorted_images


def is_right_or__left(sorted_images):
    """
    Determine if the plot is complete on the right or on the left side of the image for each element of a list of images
    :param sorted_image: dict with ndarrays containing images and info about those
    return dict of ndarrays containing images and info about those
    """
    for sorted_image in sorted_images:
        if np.sum(sorted_image["image"][:, :5, :]) > 0:
            sorted_image["is_right"] = False

        if np.sum(sorted_image["image"][:, -5:, :]) > 0:
            sorted_image["is_right"] = True

    return sorted_images


def find_best_image(sorted_images):
    """
    From a dict containing images of the same plot (and info about those images), sorted with the valid_pixel value, stitch two images to get one complete if possible, or choose the most complete one
    :param sorted_images: dict with ndarrays containing images and info about those
    :return ndarray containing the chosen or stitched image of the plot
    """
    left = False
    for sorted_image in sorted_images:
        if sorted_image["is_right"] == False:
            left = sorted_image["image"]
            left_resolution = sorted_image["resolution"]
            break

    for sorted_image in sorted_images:
        if sorted_image["is_right"]:
            right = sorted_image["image"]
            right_resolution = sorted_image["resolution"]
            if left is False:
                logging.debug(" ==> partial image kept")
                return right, right_resolution

            else:
                logging.debug(" ==> stitching in progress..")
                stitched_img = utils.stitch(left, right)
                logging.debug(" ==> done !")
                return stitched_img, left_resolution

    if left is False:
        logging.debug(" ==> partial image kept")
        return sorted_images[0]["image"], sorted_images[0]["resolution"]


def pick_image(files, extracted_plot_data):
    """
    pick an image containing the whole plot, if cannot, look for the best solution
    :param files: list of WindowsPath containing the path of all the rgb images of one plot
           extracted_plot_data: dict containing the data relative to the extraction of the plots

    :return ndarray containing the best image of the plot possible
            float containing the resolution of the image
    """
    images = utils.extract_images(files, extracted_plot_data)
    sorted_images = classify_images(images)
    if sorted_images[0]["valid_pixel"] > 99.85:
        return sorted_images[0]["image"], sorted_images[0]["resolution"]

    else:
        logging.debug(" no complete image found")
        sorted_images = detect_top_bottom_black_bars(sorted_images)
        sorted_images = is_right_or__left(sorted_images)
        return find_best_image(sorted_images)


def browse_directories(directory, extracted_plot_data_csv, output_folder, no_rotation):
    """
    browse the folders tree resulting from Phenoscript and pick or stitch the best image for each
    folder (plot) and save it in a "selected_images" folder
    :param directory: WindowsPath to access the folder containing the results of Phenoscript
           extracted_plot_data_csv: WindowsPath to acces the csv file containing the data relative to the extraction of the plots
           no_rotation: Boolean indicating if the plot rotation is forbidden
    """
    number_of_plots_extracted = 0
    extracted_plot_data = utils.from_csv_to_dict(extracted_plot_data_csv)
    list_plot_folder = os.listdir(str(directory))

    for dir in list_plot_folder:            # delete file in list_plot_folder
        if '.' in dir or "Y" not in dir or "X" not in dir:
            list_plot_folder.remove(dir)

    selected_folder = output_folder

    if len(list_plot_folder) > 0:
        for i in list_plot_folder:
            if isinstance(i, str):
                logging.debug("WORKING ON PLOT: " + i + "...")
            else:
                logging.debug("WORKING ON PLOT: " + i.name + "...")
            local_folder = directory.joinpath(i)
            files = local_folder.glob("*.tif")
            filenames = []
            for file in files:
                if '.tif.' not in str(file):
                    filenames.append(file)
            if not filenames:
                logging.debug(" no tiff found in the plot folder \n ==> Next image")
            else:
                image_s, resolution = pick_image(filenames, extracted_plot_data)
                res_name = str(int(float(resolution) * 10)) + "mm" + str(float(resolution) * 10)[2: 4]
                if no_rotation:
                    image_s = np.rot90(image_s, k=3)
                cv2.imwrite(str(selected_folder.joinpath(i + "_" + res_name + ".tif")), image_s)
                number_of_plots_extracted += 1

    else:
        raise Exception("NO PLOT DIRECTORY FOUND IN THE INDICATED LOCATION. THE plots_path ARGUMENT IS PROBABLY WRONG. "
                        "END OF PROCESS")

    if number_of_plots_extracted == 0:
        raise Exception("NO TIFF FILES FOUND IN THE FOLDER-TREE. THE plots_path ARGUMENT IS PROBABLY WRONG. "
                        "END OF PROCESS")


def browse_files(directory, extracted_plot_data_csv, output_folder):
    """
    browse the files in a plot folder, pick or stitch the best image from it and save it in a "selected_images" folder
    :param directory: WindowsPath to access the folder containing the results of Phenoscript
           extracted_plot_data_csv: WindowsPath to acces the csv file containing the data relative to the extraction of
                                    the plots
    """
    logging.debug("WORKING ON PLOT :" + directory.name + "...")
    selected_folder = output_folder
    extracted_plot_data = utils.from_csv_to_dict(extracted_plot_data_csv)
    local_folder = directory
    files = local_folder.glob("*.tif")
    filenames = []
    for file in files:
        if '.tif.' not in str(file):
            filenames.append(file)
    if not filenames:
        logging.debug(" no tiff found in the plot folder")
    else:
        image_s, resolution = pick_image(filenames, extracted_plot_data)
        res_name = str(int(float(resolution) * 10)) + "mm" + str(float(resolution) * 10)[2: 4]
        cv2.imwrite(str(selected_folder.joinpath(directory.name + "_" + res_name + ".tif")), image_s)


def run(plots_folder, extracted_plot_csv, output_folder, no_rotation):
    browse_directories(Path(plots_folder), Path(extracted_plot_csv), Path(output_folder), no_rotation)
