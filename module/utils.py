import argparse
from datetime import datetime
import csv
import logging

import cv2
import numpy as np

logger = logging.getLogger('__main__')


def extract_images(files, extracted_plot_data):
    """
    Extract data from the extracted plot data dictionnary and read image from path.
    :param files: list of path to the images of one plot
           extracted_plot_data: dict containing the extracted plots data
    :return images: dict containing images and info about those images
    """
    images = []
    for N in files:
        img = cv2.imread(str(N))
        if img.shape[0] > img.shape[1]:
            img = np.rot90(img, k=1, axes=(0, 1))
        images.append({
            "image": img,
            "resolution": float(get_image_metadata(N.name, extracted_plot_data, "resolution")),
            "valid_pixel": float(get_image_metadata(N.name, extracted_plot_data, "valid_pixel")),
            "nadir": float(get_image_metadata(N.name, extracted_plot_data, "camera_zenith")),
            "is_right": None,
            "is_cropped": False
        })

    return images


def get_image_metadata(image_name, metadata, header):
    """
    Function that get the specified metadata from the extracted plots metadata with the image_name and the feature needed?
    :param image_name: string containing the name of the image
           metadata: dict containing the extracted plots metadata
           header: string containing the feature wanted
    :return the feature value needed or False if the feature was not found
    """
    for i, img_name_dict in enumerate(metadata["image_name"]):
        if img_name_dict == image_name:
            return metadata[header][i]

    raise Exception(f"NO MATCH FOUND BETWEEN THE NAME OF THE IMAGE ANALYSED %s AND THE IMAGES DESCRIBED IN THE "
                    "'extracted_plots_data.csv' FILE" % image_name)


def from_csv_to_dict(filename):
    """
    Extract extracted plots metadata from a csv file.
    :param filename: string containing the path to the csv file
    :return dict with the important metadata
    """
    with open(filename, mode='r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        image = []
        resolution = []
        valid_pixel = []
        camera_zenith = []
        for i, row in enumerate(spamreader):
            if i == 0:
                for j, header in enumerate(row):
                    if header == "plot_id":
                        plot_col = j

                    if header == "camera_label":
                        image_col = j

                    if header == "ground_resolution":
                        resolution_col = j

                    if header == "valid_pixel":
                        valid_pixel_col = j

                    if header == "camera_zenith":
                        camera_zenith_col = j

            else:
                image.append("plot_" + row[plot_col] + "_" + row[image_col][:-4] + ".tif")
                resolution.append(row[resolution_col])
                valid_pixel.append(row[valid_pixel_col])
                camera_zenith.append(row[camera_zenith_col])

        if len(image) == 0:
            raise ValueError("THE EXTRACTED PLOT DATA CSV IS EITHER EMPTY EITHER A WRONG FILE. END OF PROCESS")

        try:
            float(valid_pixel[0])

        except ValueError:
            logging.error("THE EXTRACTED PLOT DATA CSV IS PROBABLY CORRUPTED. END OF PROCESS")

    return {"image_name": image,
            "resolution": resolution,
            "valid_pixel": valid_pixel,
            "camera_zenith": camera_zenith}


def remove_black_stripes_stitch_function(img):
    #### TEST NEEDED TO DETERMINE WHICH Rremove_black_stripes() FUNCTION IS THE BEST
    """
    crop the input image to minimize the black pixels around the microplot. Return the cropped image
    :param img: ndarray containing the image that needed to be cropped
    :return crop: ndarray containing the stitched image
    """
    black_edges = np.zeros((img.shape[0] + 200, img.shape[1] + 200, img.shape[2]))
    black_edges[100: 100 + img.shape[0], 100: 100 + img.shape[1], :] = img
    img = black_edges.astype(np.uint8)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    # plt.imshow(thresh)
    # plt.show()
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    max = 0
    for i in contours:
        if len(i) > max:
            max = len(i)
            cnt = i

    x, y, w, h = cv2.boundingRect(cnt)
    crop = img[y:y + h, x:x + w]
    return crop


def stitch(img2, img1):
    """stitch two images containing each one an extremity of the plot
    :param img1: ndarray containing the image of the left extremity of the plot
    :param img2: ndarray containing the image of the right extremity of the plot
    return result: ndarray containing the image stitched with the two input images
    """
    # ********************** mettre un if la rotation est nécessaire (sounds useless)
    img2 = remove_black_stripes_stitch_function(img2)
    sift1 = cv2.SIFT_create()
    (kp1, features1) = sift1.detectAndCompute(img1, None)
    kp1 = np.float32([kp.pt for kp in kp1])

    sift2 = cv2.SIFT_create()
    (kp2, features2) = sift2.detectAndCompute(img2, None)
    kp2 = np.float32([kp.pt for kp in kp2])

    matcher = cv2.DescriptorMatcher_create("BruteForce")
    rawMatches = matcher.knnMatch(features1, features2, 2)
    matches = []
    ratio = 0.75
    reprojThresh = 10.0
    for m in rawMatches:
        if len(m) == 2 and m[0].distance < m[1].distance * ratio:
            matches.append((m[0].trainIdx, m[0].queryIdx))

    # computing a homography requires at least 4 matches
    if len(matches) > 4:
        # construct the two sets of points
        ptsA = np.float32([kp1[i] for (_, i) in matches])
        ptsB = np.float32([kp2[i] for (i, _) in matches])

        # compute the homography between the two sets of points
        (H, status) = cv2.findHomography(ptsA, ptsB, cv2.RANSAC,
                                         reprojThresh)

    # **** gestion d'erreur sur le H *****
    result = cv2.warpPerspective(img1, H, (int(img1.shape[1] * 1.5), int(img1.shape[0] * 1.5)))
    result[0:img2.shape[0], 0:int(img2.shape[1] / 2)] = img2[:, :int(img2.shape[1] / 2)]

    result = remove_black_stripes_stitch_function(result)
    return result
