import unittest
from pathlib import Path

from module import uav_rgb_imageselection_all


class TestCore(unittest.TestCase):
    """
    Test case for core methods

    TODO: Add other test cases. For help refer to this tutorial: https://docs.python.org/3/library/unittest.html
    """

    def test_run(self):
        uav_rgb_imageselection_all.run(output_folder=Path("../data/output"),
                                       extracted_plot_csv=Path("../data/21TE41/extracted_plots_data.csv"),
                                       plots_folder=Path("../data/21TE41"))


if __name__ == '__main__':
    unittest.main()
