"""
Command Line Interface (CLI) manager for My Module.

TODO: Adds description
"""
import argparse
import logging
import os
import time
import sys
from pathlib import Path

from module import uav_rgb_imageselection_all
from module import error

PROCESS_NAME = "UAV RGB Image Selection All"


def create_parser():
    """Create Command Line Interface arguments parser
     """
    parser = argparse.ArgumentParser()

    # Positional arguments, declaration order is important
    parser.add_argument(
        "output_folder",
        help="path to the directory where the output will be generated")

    parser.add_argument(
        'plots_path',
        help='path to the directory where to find the images processed with Phenoscript'
    )

    parser.add_argument(
        'plots_data',
        help='path to the corresponding "extracted_plot_data.csv"'
    )

    parser.add_argument(
        "-n",
        "--no_rotation",
        help="do not rotate the uplot when vertical",
        action="store_true",
        default=False)

    parser.add_argument(
        "-v",
        "--verbose",
        help="activate output verbosity",
        action="store_true",
        default=False)

    return parser.parse_args()


if __name__ == '__main__':
    timer_start = time.time()
    try:
        # Manage CLI arguments
        args = create_parser()

        # Init Logger
        logger = logging.getLogger()

        if args.verbose:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.addHandler(logging.FileHandler(os.path.splitext(__file__)[0] + '.log', 'w'))

        logging.debug("CLI arguments are valid")

        # Creating output folder if doesn't exist
        if not os.path.exists(args.output_folder):
            logging.debug("Create output folder '" + args.output_folder + "'")
            Path(args.output_folder).mkdir(exist_ok=True, parents=True)

        # Run module core process
        uav_rgb_imageselection_all.run(args.plots_path, args.plots_data, args.output_folder, args.no_rotation)

        logging.info("Process '" + PROCESS_NAME + "' complete")

    except error.DataError as e:
        # When DataError is raised, the process fails without killing the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
    except Exception as e:
        # Every other errors kill the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
        sys.exit(1)
    finally:
        timer_end = time.time()
        logging.debug("Process '" + PROCESS_NAME + "' took " + str(int(timer_end - timer_start)) + " seconds")
